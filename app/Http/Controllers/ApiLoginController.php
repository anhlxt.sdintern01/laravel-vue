<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiRequestLogin;
use App\Http\Requests\ApiRequestRegister;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiLoginController extends Controller
{
    private $user;
    public function __construct(User $user)
    {
        $this->user=$user;
    }
    public function register(ApiRequestRegister $request){
       $this->user->create([
        'email'=>$request->email,
        'password'=>$request->password,
        'name'=>$request->name,
       ]);
       return response()->json(['success'=>'Register Success'],200);
    }
    public function login(ApiRequestLogin $request){
        if(Auth::attempt([
            'email'=>$request->email,
            'password'=>$request->password,
        ])){
            $user=$this->user->whereEmail($request->email)->first();
            $user->token=$user->createToken('App')->accessToken;
            return response()->json($user);
        }
        return response()->json(['error','tai khoan mat khau sai'],500);
    }
    public function userInfor(Request $request){
        return response()->json($request->user('api'));
    }
}
