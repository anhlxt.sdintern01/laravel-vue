<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_questions_answers', function (Blueprint $table) {
            $table->id();
            $table->json('choice');
            $table->rememberToken();
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('question_survey_id');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');
            $table->foreign('question_survey_id')
                  ->references('id')
                  ->on('questions_surveys');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_questions_answers');
    }
};
